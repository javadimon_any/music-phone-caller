package ru.anyprojects.musicphonecaller.listeners;

import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

public class PhoneListener extends PhoneStateListener {

    private final Context context;
    private int prevState;

    public PhoneListener(Context context){
        this.context = context;
    }

    public void onCallStateChanged(int state, String incomingNumber) {

        switch (state) {
            case TelephonyManager.CALL_STATE_IDLE:
                Log.d("DEBUG", "IDLE " + prevState);

                if((prevState == TelephonyManager.CALL_STATE_OFFHOOK)){
                    Log.d("DEBUG LISTENER", "CALL ENDED");

                    Intent intent = new Intent();
                    intent.setAction("ru.anyprojects.musicphonecaller.services.MusicService");
                    intent.putExtra("Command", "Stop");
                    context.sendBroadcast(intent);
                }

                if((prevState == TelephonyManager.CALL_STATE_RINGING)){
                    Log.d("DEBUG LISTENER", "CALL REJECTED");

                    Intent intent = new Intent();
                    intent.setAction("ru.anyprojects.musicphonecaller.services.MusicService");
                    intent.putExtra("Command", "Stop");
                    context.sendBroadcast(intent);
                }

                prevState = TelephonyManager.CALL_STATE_IDLE;

                break;

            case TelephonyManager.CALL_STATE_OFFHOOK:
                Log.d("DEBUG", "OFFHOOK");
                prevState = TelephonyManager.CALL_STATE_OFFHOOK;
                Intent intent = new Intent();
                intent.setAction("ru.anyprojects.musicphonecaller.services.MusicService");
                intent.putExtra("Command", "StartMusic");
                context.sendBroadcast(intent);
                break;

            case TelephonyManager.CALL_STATE_RINGING:
                Log.d("DEBUG", "RINGING " + incomingNumber);
                prevState = TelephonyManager.CALL_STATE_RINGING;
                // TODO
                break;
        }
    }
}
