package ru.anyprojects.musicphonecaller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import ru.anyprojects.musicphonecaller.listeners.PhoneListener;
import ru.anyprojects.musicphonecaller.services.MusicService;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_PHONE_STATE;

public class MainActivity extends AppCompatActivity {

    private TelephonyManager telephony;
    private PhoneListener phoneListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        phoneListener = new PhoneListener(this);
        telephony = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String command = intent.getStringExtra("Command");
                Log.d("MusicService DEBUG", command);

                if(command == null){
                    return;
                }

                switch (command){
                    case "Stop":
                        telephony.listen(null, PhoneStateListener.LISTEN_NONE);
                        break;
                }
            }
        };

        Intent musicServiceIntent = new Intent(this, MusicService.class);
        startService(musicServiceIntent);

        IntentFilter filter = new IntentFilter();
        filter.addAction("ru.anyprojects.musicphonecaller.services.MusicService");
        registerReceiver(broadcastReceiver, filter);

        final Button btnCall = findViewById(R.id.btnCall);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissionsAndStart();
            }
        });
        btnCall.setEnabled(false);

        EditText phoneNumber = findViewById(R.id.editText);
        phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                boolean isEnable = false;
                if(charSequence.toString().startsWith("7") && charSequence.length() == 11){
                    isEnable = true;
                }
                btnCall.setEnabled(isEnable);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //
            }
        });
    }

    private Context getCurrentContext(){
        return this;
    }

    private void checkPermissionsAndStart(){
        if (ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            startServices();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{CALL_PHONE, READ_PHONE_STATE}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        int successPermissions = 0;
        int index = 0;
        for(int result : grantResults){
            if (result == PackageManager.PERMISSION_GRANTED) {
                successPermissions++;

            } else{
                Log.d("MainActivity", permissions[index] + " permission not granted");
                Toast.makeText(this, permissions[index] + " permission not granted", Toast.LENGTH_SHORT).show();
            }

            index++;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(successPermissions == permissions.length){
            startServices();
        } else {
            Toast.makeText(this, "Not all permission granted", Toast.LENGTH_SHORT).show();
        }
    }

    private void startServices(){

        //telephony.listen(phoneListener, PhoneStateListener.LISTEN_SERVICE_STATE | PhoneStateListener.LISTEN_CALL_STATE);

        Intent musicServiceIntent = new Intent(this, MusicService.class);
        startService(musicServiceIntent);

        // This hack not working
        //Intent callDetectionIntent = new Intent(this, CallDetection.class);
        //startService(callDetectionIntent);

        EditText phoneNumber = findViewById(R.id.editText);
        String phone = "+" + phoneNumber.getText();
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        telephony.listen(null, PhoneStateListener.LISTEN_NONE);

        Intent intent = new Intent();
        intent.setAction("ru.anyprojects.musicphonecaller.services.MusicService");
        intent.putExtra("Command", "Stop");
        sendBroadcast(intent);


    }
}
